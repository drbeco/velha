#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SBUF 32

/* retorna verdadeiro se ha uma casa livre */
int casa_livre(char tab[10]);
/* retorna verdadeiro se o jogador da vez dada ganhou no tabuleiro tab */
int ganhou_tal(char tab[10], char vez);
/* calcula jogada do computador */
void compjoga(char tab[10], int nivel);
/* recebe jogada do humano */
void humajoga(char tab[10]);
/* imprime o tabuleiro */
void imptab(char tab[10]);

/* algoritmo recursivo minimax */
void minimax(char tab[10], int prof, int nivel, int tuso, int tpas);
/* retorna verdadeiro se a profundidade ja foi o suficiente */
int profsuf(int prof, int nivel);
/* retorna o valor do tabuleiro do ponto de vista do jogador */
int estatico(char atual[10], int prof);
/* calcula os lances sucessores de um dado tabuleiro */
void geramov(char atual[10], int succ[10]);
/* copia vetor de 10 inteiros */
void copy10(int a[10], int b[10]);

struct resultado
{
    int valor;
    int caminho[9];
};
struct resultado result;

/*  variavel para o nivel maximo de profundidade na busca */
char primeiro = 'c', segundo = 'h';

int main(void)
{
    /*       7   |   2   |   3       humano:H */
    /*    -------+-------+-------    comput:C */
    /*       0   |   4   |   8       vazio :espaco */
    /*    -------+-------+------- */
    /*       5   |   6   |   1 */
    char tab[10];
    char vez;
    int nivel;
    char sgt[SBUF];

    do
    {
        strcpy(tab, "         ");
        printf("\nQuem comeca? (c/h): ");
        fgets(sgt, SBUF, stdin);
        primeiro = sgt[0];
        segundo = 'c';
        if(primeiro == 'c') segundo = 'h';
        if(segundo == 'c') primeiro = 'h';
        printf("\nPrimeiro %c (X). Segundo %c (O)\n", primeiro, segundo);

        printf("\nQual a dificuldade (1-9): ");
        fgets(sgt, SBUF, stdin);
        nivel = strtol(sgt, NULL, 10);
        if(nivel > 9) nivel = 9;
        if(nivel < 1) nivel = 1;
        printf("\nDificuldade: %d\n", nivel);
        
        printf("\n\n\n\n");
        printf("\n 7 | 2 | 3");
        printf("\n---+---+---");
        printf("\n 0 | 4 | 8");
        printf("\n---+---+---");
        printf("\n 5 | 6 | 1");
        vez = primeiro;
        do
        {
            if(vez == 'c')
            {
                printf("\n\n\n\n");
                compjoga(tab, nivel);
                printf("\n");
                imptab(tab);
                vez = 'h';
            }
            else
            {
                humajoga(tab);
                vez = 'c';
            }
            if(ganhou_tal(tab, 'c') || ganhou_tal(tab, 'h'))
            {
                if(ganhou_tal(tab, 'c'))
                    printf("\nComputador Ganhou.");
                else
                    printf("\nHumano Ganhou.");
                getchar();
                break;
            }
        } while(casa_livre(tab));

        if(!ganhou_tal(tab, 'c') && !ganhou_tal(tab, 'h'))
        {
            printf("\nEmpate.");
            getchar();
        }
        printf("\nQuer jogar de novo (s/n): ");
        fgets(sgt, SBUF, stdin);
        vez = sgt[0];
    } while(vez == 's');
}

/* retorna verdadeiro se ha uma casa livre */
int casa_livre(char tab[10])
{
    int i;
    for(i = 0; i < 9; i++)
        if(tab[i] == ' ')
            return 1;
    return 0;
}

/* retorna verdadeiro se o jogador da vez dada ganhou no tabuleiro tab */
int ganhou_tal(char tab[10], char vez)
{
    int a, b, c;

    for(a = 0; a < 7; a++)
    {
        if(tab[a] != vez)
            continue;
        for(b = a + 1; b < 8; b++)
        {
            if(tab[b] != vez)
                continue;
            for(c = b + 1; c < 9; c++)
            {
                if(tab[c] != vez)
                    continue;
                if(a + b + c == 12)
                    return 1;
            }     /* for c */
        }       /* for b */
    }         /* for a */
    return 0;
}

/* recebe jogada do humano */
void humajoga(char tab[10])
{
    int numcasa;
    char scasa[SBUF];

    while(1)
    {
        printf("\nJogou em qual casa? (0..8): ");
        fgets(scasa, SBUF, stdin);
        numcasa = strtol(scasa, NULL, 10);
        if(numcasa<0 || numcasa>8)
            continue;
        if(tab[numcasa] != ' ')
            printf("\nEsta casa ja esta ocupada por %c.", tab[numcasa]);
        else
            break;
    }
    tab[numcasa] = 'h';
}

/* calcula jogada do computador */
void compjoga(char tab[10], int nivel)
{
    int i;

    minimax(tab, 0, nivel, 11, -11);   /* retorna o melhor caminho a partir de tab */
    if(result.caminho[0] == -1) /* profundidade zero, limite maximo de estatico */
    {
        /* limite minimo de estatico */
        printf("Erro. Nao tenho mais jogadas.");
        exit(0);
    }
    tab[result.caminho[0]] = 'c';
    printf("Joguei na casa: %d", result.caminho[0]);
    printf("\nValor do lance: %d", result.valor);
    printf("\nA melhor variante calculada: ");
    for(i = 0; i < 9; i++)
        if(result.caminho[i] == -1)
            break;
        else
            printf("%d ", result.caminho[i]);
}

/* imprime o tabuleiro */
void imptab(char tab[10])
{
    int ordem[9] = {7, 2, 3, 0, 4, 8, 5, 6, 1};
    int linha, col, indord = 0;
    for(linha = 0; linha < 3; linha++)
    {
        for(col = 0; col < 3; col++)
        {
            if(tab[ordem[indord]] != ' ')
                printf(" %c", tab[ordem[indord]]);
            else
                printf(" %d", ordem[indord]);
            if((indord + 1) % 3)
                printf(" |");
            indord++;
        }
        if(linha != 2)
            printf("\n---+---+---\n");
    }
}

/* algoritmo recursivo minimax */
void minimax(char atual[10], int prof, int nivel, int tuso, int tpas)
{
    int succ[10], i, novo_valor, aux;
    char tab[10];
    int melhor_caminho[10];

    if(profsuf(prof, nivel)) /*  profundidade suficiente */
    {
        result.caminho[0] = -1; /* caracter nulo no vetor caminho */
        result.valor = estatico(atual, prof);
        return;
    }
    geramov(atual, succ);   /* succ[0]=-1 se alguem ganhou ou empatou */
    if(succ[0] == -1)
    {
        result.caminho[0] = -1;
        result.valor = estatico(atual, prof);
        return;
    }
    for(i = 0; succ[i] != -1; i++)
    {
        strcpy(tab, atual);
        if(prof % 2 == 0)
            tab[succ[i]] = 'c';
        else
            tab[succ[i]] = 'h';
        minimax(tab, prof + 1, nivel, -tpas, -tuso);
        novo_valor = -result.valor;
        if(novo_valor > tpas)
        {
            tpas = novo_valor;
            melhor_caminho[0] = succ[i];
            for(aux = 0; aux < 9; aux++)
                melhor_caminho[aux + 1] = result.caminho[aux];
        }
    }
    result.valor = tpas;
    copy10(result.caminho, melhor_caminho);
}

/* retorna verdadeiro se a profundidade ja foi o suficiente */
int profsuf(int prof, int nivel)
{
    if(prof == nivel)
        return 1;
    return 0;
}

/* retorna o valor do tabuleiro do ponto de vista do jogador */
int estatico(char tab[10], int prof)
{
    int l_comp = 0, l_huma = 0;
    int a, b, c;
    char vez = 'c';

    if(prof % 2 != 0)     /* profundidade impar */
    {
        if(ganhou_tal(tab, 'c'))
            return -10;
        if(ganhou_tal(tab, 'h'))
            return 10;
    }
    else   /* profundidade par. Quem vai jogar e' o comp */
    {
        if(ganhou_tal(tab, 'c'))
            return 10;
        if(ganhou_tal(tab, 'h'))
            return -10;
    }

    do
    {
        for(a = 0; a < 7; a++)
            for(b = a + 1; b < 8; b++)
                for(c = b + 1; c < 9; c++)
                {
                    if(a + b + c != 12)
                        continue;
                    if(tab[a] != vez && tab[b] != vez && tab[c] != vez)
                    {
                        if(vez == 'h')
                            l_comp++;
                        else
                            l_huma++;
                    }
                }
        vez += 5;
    } while(vez == 'h');

    if(prof % 2 == 0)
        return l_comp - l_huma;
    else
        return l_huma - l_comp;
}

/* calcula os lances sucessores de um dado tabuleiro */
void geramov(char tab[10], int succ[10])
{
    int i, j = 0;

    succ[0] = -1;
    if(ganhou_tal(tab, 'c') || ganhou_tal(tab, 'h'))
        return;
    if(!casa_livre(tab))
        return;
    for(i = 0; i < 9; i++)
        if(tab[i] == ' ')
            succ[j++] = i;
    succ[j] = -1;
}

/* copia vetor de 10 inteiros */
void copy10(int a[10], int b[10])
{
    int i;
    for(i = 0; i < 10; i++)
        a[i] = b[i];
}

